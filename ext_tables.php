<?php
defined('TYPO3_MODE') or die();

call_user_func(
    function ($extKey, $camelCaseExtKey) {
        if (TYPO3_MODE !== 'BE') return;

        // Add Debug Module-Section
        $GLOBALS['TBE_MODULES']['debug'] = '';
        $GLOBALS['TBE_MODULES']['_configuration']['debug'] = [
            'labels' => 'LLL:EXT:' .$extKey. '/Resources/Private/Language/locallang_mod_debug.xlf',
            'name' => 'debug',
            'iconIdentifier' => 'module-system'
        ];

        // Register the page tree core navigation component
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addCoreNavigationComponent(
            'debug',
            'TYPO3/CMS/Backend/PageTree/PageTreeElement'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
            'TYPOworx.TnmTsconfigBrowser',
            'debug',
            'Module',
            '',
            [
                'Configuration' => 'main',
            ],
            [
                'access' => 'admin',
                'icon' => 'EXT:' .$extKey. '/Resources/Public/Icons/Extension.png',
                'labels' => 'LLL:EXT:' .$extKey. '/Resources/Private/Language/locallang_mod_configuration.xlf',
                'navigationComponentId' => 'TYPO3/CMS/Backend/PageTree/PageTreeElement'
            ]
        );
    },
    'tnm_tsconfig_browser',  'TnmTsconfigBrowser'
);
