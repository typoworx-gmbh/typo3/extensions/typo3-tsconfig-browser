<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'System > TSconfig Browser',
    'description' => 'Enables the \'Config\' and \'DB Check\' modules for technical analysis of the system. This includes raw database search, checking relations, counting pages and records etc.',
    'category' => 'module',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'author' => 'TYPO3 Core Team',
    'author_email' => 'typo3cms@typo3.org',
    'author_company' => '',
    'version' => '9.0.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.20',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
