<?php
declare(strict_types=1);
namespace TYPOworx\TnmTsconfigBrowser\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Response;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;
use TYPO3\CMS\Lowlevel\Utility\ArrayBrowser;
use TYPO3\CMS\Backend\Template\ModuleTemplate;
use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Http\ServerRequestFactory;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * View configuration arrays in the backend
 * @internal This class is a specific Backend controller implementation and is not part of the TYPO3's Core API.
 */
class ConfigurationController extends ActionController
{
    /**
     * @var int
     */
    protected $pageId = 0;

    /**
     * Available trees to render.
     *  * label is an LLL identifier
     *  * type is used to identify the data source type
     *  * globalKey (only for type=global) is the name of a global variable
     *
     * @var array
     */
    protected $treeSetup = [
        'pageTS' => [
            'label' => 'pageTS',
            'type' => 'global',
            'globalKey' => 'pageTS',
        ],
        'userTS' => [
            'label' => 'userTS',
            'type' => 'global',
            'globalKey' => 'userTS',
        ],
    ];


    /**
     * Main controller action determines get/post values, takes care of
     * stored backend user settings for this module, determines tree
     * and renders it.
     */
    public function mainAction()
    {
        $this->pageId = (int)GeneralUtility::_GET('id');

        $backendUser = $this->getBackendUser();
        $languageService = $this->getLanguageService();

        $serverRequest = ServerRequestFactory::fromGlobals();

        $queryParams = $serverRequest->getQueryParams();
        $postValues = $serverRequest->getParsedBody();

        $moduleState = $backendUser->uc['moduleData']['tnm_tsconfig_browser'] ?? [];

        // Determine validated tree key and tree detail setup
        $selectedTreeKey = $this->treeSetup[$queryParams['tree']] ? $queryParams['tree'] : ($this->treeSetup[$moduleState['tree']] ? $moduleState['tree'] : key($this->treeSetup));
        $selectedTreeDetails = $this->treeSetup[$selectedTreeKey];
        $moduleState['tree'] = $selectedTreeKey;

        // Search string given or regex search enabled?
        $searchString = (string)($postValues['searchString'] ? trim($postValues['searchString']) : '');
        $moduleState['regexSearch'] = (bool)($postValues['regexSearch'] ?? $moduleState['regexSearch'] ?? false);

        // Prepare main array
        $sortKeysByName = true;

        if ($selectedTreeDetails['type'] === 'global')
        {
            $selectedTreeDetails['type'] = key($this->treeSetup);
        }

        if ($selectedTreeDetails['type'] === 'pageTS')
        {
            $renderArray = BackendUtility::getPagesTSconfig($this->pageId);
        }
        elseif ($selectedTreeDetails['type'] === 'userTS')
        {
            $renderArray = $backendUser->getTSConfig();
        }
        else
        {
            throw new \RuntimeException('Unknown array type "' . $selectedTreeDetails['type'] . '"', 1507845662);
        }

        if ($sortKeysByName === true)
        {
            ArrayUtility::naturalKeySortRecursive($renderArray);
        }

        // Prepare array renderer class, apply search and expand / collapse states
        $arrayBrowser = GeneralUtility::makeInstance(ArrayBrowser::class);
        $arrayBrowser->dontLinkVar = true;
        $arrayBrowser->searchKeysToo = true;
        $arrayBrowser->regexMode = $moduleState['regexSearch'];
        $node = $queryParams['node'];

        if ($searchString)
        {
            $arrayBrowser->depthKeys = $arrayBrowser->getSearchKeys($renderArray, '', $searchString, []);
        }
        elseif (is_array($node))
        {
            $newExpandCollapse = $arrayBrowser->depthKeys($node, $moduleState['node_' . $selectedTreeKey]);
            $arrayBrowser->depthKeys = $newExpandCollapse;
            $moduleState['node_' . $selectedTreeKey] = $newExpandCollapse;
        }
        else
        {
            $arrayBrowser->depthKeys = $moduleState['node_' . $selectedTreeKey] ?? [];
        }

        // Store new state
        $backendUser->uc['moduleData']['tnm_tsconfig_browser'] = $moduleState;
        $backendUser->writeUC();

        // Render main body
        $view = GeneralUtility::makeInstance(StandaloneView::class);
        $view->getRequest()->setControllerExtensionName('tnm_tsconfig_browser');
        $view->setTemplatePathAndFilename(GeneralUtility::getFileAbsFileName('EXT:tnm_tsconfig_browser/Resources/Private/Backend/Templates/Configuration.html'));
        $view->assignMultiple([
            'treeName' => $selectedTreeDetails['label'],
            'searchString' => $searchString,
            'regexSearch' => $moduleState['regexSearch'],
            'tree' => $arrayBrowser->tree($renderArray, ''),
        ]);

        // Prepare module setup
        $moduleTemplate = GeneralUtility::makeInstance(ModuleTemplate::class);
        $moduleTemplate->setContent($view->render());
        $moduleTemplate->getPageRenderer()->loadRequireJsModule('TYPO3/CMS/Lowlevel/ConfigurationView');

        // Shortcut in doc header
        $shortcutButton = $moduleTemplate->getDocHeaderComponent()->getButtonBar()->makeShortcutButton();
        $shortcutButton
            ->setModuleName('tnm_tsconfig_browser')
            ->setDisplayName(
                $languageService->sL('LLL:EXT:tnm_tsconfig_browser/Resources/Private/Language/locallang.xlf:' . $selectedTreeDetails['label'])
            )
            ->setSetVariables(['tree'])
        ;
        $moduleTemplate->getDocHeaderComponent()->getButtonBar()->addButton($shortcutButton);

        // Main drop down in doc header
        $menu = $moduleTemplate->getDocHeaderComponent()->getMenuRegistry()->makeMenu();
        $menu->setIdentifier('tree');

        foreach ($this->treeSetup as $treeKey => $treeDetails)
        {
            $menuItem = $menu->makeMenuItem();

            /** @var \TYPO3\CMS\Backend\Routing\UriBuilder $uriBuilder */
            $uriBuilder = GeneralUtility::makeInstance(\TYPO3\CMS\Backend\Routing\UriBuilder::class);
            $menuItem
                ->setHref((string)$uriBuilder->buildUriFromRoute(
                    'debug_TnmTsconfigBrowserModule',
                    ['tree' => $treeKey])
                )
                ->setTitle(
                    $languageService->sL(
                        'LLL:EXT:tnm_tsconfig_browser/Resources/Private/Language/locallang.xlf:' . $treeDetails['label'])
                )
            ;

            if ($selectedTreeKey === $treeKey)
            {
                $menuItem->setActive(true);
            }

            $menu->addMenuItem($menuItem);
        }
        $moduleTemplate->getDocHeaderComponent()->getMenuRegistry()->addMenu($menu);

        $this->view = null;
        $response = new Response();
        $response->setContent($moduleTemplate->renderContent());

        return $response;
    }

    /**
     * Returns the Backend User
     * @return BackendUserAuthentication
     */
    protected function getBackendUser()
    {
        return $GLOBALS['BE_USER'];
    }

    /**
     * @return LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }
}
