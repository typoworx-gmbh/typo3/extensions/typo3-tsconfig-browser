<?php
defined('TYPO3_MODE') or die();

call_user_func(
    function ($extKey) {
        if (TYPO3_MODE !== 'BE') return;

        $moduleName = 'tx_tnmtsconfigbrowser';

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScriptSetup(
            'plugin.' . $moduleName .' {
                mvc.callDefaultActionIfActionCantBeResolved = 1
            }

            module.' . $moduleName .' {
                view {
                    templateRootPaths {
                        0 = EXT:' .$extKey. '/Resources/Private/Backend/Templates/
                        1 = {$module.' . $moduleName .'.view.templateRootPath}
                    }
                    partialRootPaths {
                        0 = EXT:' .$extKey. '/Resources/Private/Backend/Partials/
                        1 = {$module.' . $moduleName .'.view.partialRootPath}
                    }
                    layoutRootPaths {
                        0 = EXT:' .$extKey. '/Resources/Private/Backend/Layouts/
                        1 = {$module.' . $moduleName .'.view.layoutRootPath}
                    }
                }
                persistence < plugin.' . $moduleName .'.persistence
            }'
        );
    },
    'tnm_tsconfig_browser'
);
